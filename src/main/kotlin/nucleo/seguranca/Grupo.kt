/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.seguranca

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.SoftDelete
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class Grupo : CustomModel() {
    @Id
    @GeneratedValue
    override var id: Int? = null
    var nome: String? = null

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    var autorizacoes: List<Autorizacao>? = null

    @SoftDelete
    @JsonIgnore
    var excluido = false

    @ManyToMany(mappedBy = "grupos")
    var pessoas: List<Pessoa>? = null
}