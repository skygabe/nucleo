/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.seguranca

import br.com.hs1.morada.nucleo.CustomModel
import br.com.hs1.morada.nucleo.prensa.Veiculo
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.NotNull
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.util.*
import javax.persistence.*

@Entity
data class Pessoa(
        @Id
        @GeneratedValue
        override //(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        @NotNull
        var nome: String? = null,

        @Index(unique = true)
        var login: String? = null,

        @JsonIgnore
        var senha: String = "123",
        var senhaExpirada: Boolean = false,
        var celular: String? = null,
        var cpf: String? = null,
        var email: String? = null,

        @SoftDelete
        @JsonIgnore
        var excluido: Boolean = false,

        @JsonIgnore
        @Temporal(TemporalType.TIMESTAMP)
        @WhenModified
        var ultimaAtualizacao: Date? = null,

        @ManyToMany
        var grupos: List<Grupo>? = null,

        @Column(length = 100)
        var cargo: String? = null,

        @Column(length = 50)
        var apelido: String? = null,

//    @ElementCollection
//    private List<Funcao> funcoes;
        @ManyToMany
        var veiculos: List<Veiculo>? = null

) : CustomModel(){
    override fun toString() = nome!!
}