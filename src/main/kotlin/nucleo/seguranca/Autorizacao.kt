/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.seguranca

import java.io.Serializable
import javax.persistence.*

@Entity
class Autorizacao : Serializable {
    @Id
    var id = 0

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    var transacao: Transacao? = null

    @ManyToOne
    var grupo: Grupo? = null
    var acessar = false
    var incluir = false
    var excluir = false
    var alterar = false
    var permissoes: String?
        get() = (if (incluir) "I" else "") + (if (excluir) "E" else "") + (if (alterar) "A" else "") + if (acessar) "V" else ""
        set(perms) {
            var perms = perms
            if (perms == null) {
                perms = ""
            }
            incluir = perms.contains("I")
            excluir = perms.contains("A")
            alterar = perms.contains("E")
            acessar = perms.contains("V")
        }

    fun addPermissoes(perms: String?): Autorizacao {
        permissoes = perms?:"" + permissoes
        return this
    }

    override fun toString() =  transacao!!.name + ":" + permissoes
}