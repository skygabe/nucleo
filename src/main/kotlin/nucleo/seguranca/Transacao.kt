/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.seguranca

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class Transacao(val descricao: String) {
    PESSOA("pessoa"),
    VEICULO("veiculo"),
    CHECKLIST("checklist"),
    DISPOSITIVO("dispositivo"),
    MARCAMODELO("marcaModelo"),
    FORNECEDOR("fornecedor"),
    MATERIAL("material"),
    TIPO_MANUTENCAO("tipoManutencao"),
    ATIVIDADE("atividade"),
    COMPONENTE("componente"),
    JORNADA("jornada"),
    GRUPO("grupo"),
    REL_ATIVIDADE_EXECUTADA("atividades_executadas"),
    REL_PACOTES("pacotes"),
    FOLHA_PONTO("folhaPonto"),
    PERCURSO("percurso"),
    PLANILHA("excel"),
    TABLET("tablet"),
    QUESTIONARIO_PERIODO("questionario_periodo"),
    QUESTIONARIO("questionario");


    fun getCodigo(): String {
        return name
    }

    companion object {
        fun findByUrl(url: String): Transacao? {
            for (t in values()) {
                if (t.descricao == url) {
                    return t
                }
            }
            return null
        }
    }
}