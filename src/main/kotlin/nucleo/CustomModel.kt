package br.com.hs1.morada.nucleo

import io.ebean.DB
import io.ebean.Model
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class CustomModel : Model() {
    abstract val id: Int?

    fun toJSON() = DB.json().toJson(this)
}