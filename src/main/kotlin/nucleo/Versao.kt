/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo

import io.ebean.annotation.Aggregation
import io.ebean.annotation.View
import java.util.*
import javax.persistence.Entity
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 *
 * @author jcarlos
 */
@View(name = "view_versao")
@Entity
class Versao {
    val nome: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    val versao: Date? = null

    @Aggregation("max(versao)")
    var maxVersao: Date? = null
}