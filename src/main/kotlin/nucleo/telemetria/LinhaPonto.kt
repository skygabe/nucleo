/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class LinhaPonto : Serializable {
    @Id
    @GeneratedValue //(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @Column(length = 2)
    var dia: String? = null

    @Column(length = 3)
    var semana: String? = null
    var marcacoes = ""

    @ManyToOne
    @JsonIgnore
    var folhaPonto: FolhaPonto? = null

    @Enumerated
    var situacao: Situacao = Situacao.NORMAL

    constructor() {}
    constructor(data: Date?, sit: Situacao) : this(data) {
        situacao = sit
    }

    private constructor(data: Date?) {
        dia = DIA_MES.format(data)
        semana = DIA_SEMANA.format(data).capitalize()
        println("semana: $semana")
    }

    constructor(jorn: Jornada) : this(jorn.inicio) {}

    fun addTrabalho(ini: Date?, fim: Date?) {
//        this.trabalho.add(ini, fim);
        if (!marcacoes.isEmpty()) {
            marcacoes += " "
        }
        marcacoes += HORA_MINUTO.format(ini) + " " + HORA_MINUTO.format(fim)
    }
    companion object {
        private val DIA_MES = SimpleDateFormat("dd", Locale("pt", "BR"))
        private val DIA_SEMANA = SimpleDateFormat("EEE", Locale("pt", "BR"))
        private val HORA_MINUTO = SimpleDateFormat("HH:mm")
    }
}
