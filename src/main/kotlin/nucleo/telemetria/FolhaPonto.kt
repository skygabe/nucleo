/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.CustomModel
import br.com.hs1.morada.nucleo.seguranca.Pessoa
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class FolhaPonto : CustomModel {
    @Id
    @GeneratedValue
    override //(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @Temporal(TemporalType.DATE)
    val inicio: Date?

    @Temporal(TemporalType.DATE)
    val fim: Date?
    val empregador: String?
    val cnpj: String?
    val endereco: String?
    var matricula: String? = null
    var funcionario: String? = null
    var apelido: String? = null
    var cargo: String? = null

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "folhaPonto")
    @OrderBy("id")
    var linhas: MutableList<LinhaPonto> = ArrayList()

    constructor() {
        inicio = null
        fim = null
        empregador = null
        cnpj = null
        endereco = null
    }

    fun addPonto(pto: LinhaPonto) {
        pto.folhaPonto = this
        linhas.add(pto)
    }

    constructor(config: Configuracao, inicio: Date?, fim: Date?, operador: Pessoa) {
        empregador = config.empregador
        cnpj = config.cnpj
        endereco = config.endereco
        this.inicio = inicio
        this.fim = fim
        funcionario = operador.nome
        matricula = operador.login
        apelido = operador.apelido
        cargo = operador.cargo
        println("FOLHA DE $empregador")
    }
}