/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.CustomModel
import br.com.hs1.morada.nucleo.prensa.Fornecedor
import br.com.hs1.morada.nucleo.prensa.ItemCheckList
import br.com.hs1.morada.nucleo.prensa.Veiculo
import br.com.hs1.morada.nucleo.seguranca.Pessoa
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Formula
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
class Jornada : CustomModel(), Serializable, Sincronizavel {
    @Id
    @GeneratedValue
    override //(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @ManyToOne
    var motorista: Pessoa? = null
    var codigoData: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    var inicio: Date? = null

    @Temporal(TemporalType.TIMESTAMP)
    var fim: Date? = null
    var numControleJornada: String? = null

    @ManyToMany(cascade = [CascadeType.ALL])
    var checkList: List<ItemCheckList> = ArrayList()

    @Column(length = 2000)
    var obs: String? = null
    var horimetro: Int? = null
    var hodometro: Int? = null
    var nivelOleo: Int? = null

    @ManyToOne
    var fornecedor: Fornecedor? = null

    @ManyToOne
    var veiculo: Veiculo? = null
    var idJornada: Long? = null
    var totalPacotes = 0

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "jornada")
    @OrderBy("inicio")
    var atividades: List<AtividadeExecutada>? = null

    @SoftDelete
    @JsonIgnore
    override var excluido = false

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    @JsonIgnore
    var ultimaAtualizacao: Date? = null

    //null -> indica que precisa sincronizar com a ARCELOR
    //true -> Não precisa sincronizar
   override var sincronizado: Boolean? = null
    override var enviado: Boolean? = null

    /*
           ENVIADO         EXCLUIDO        ACAO
           true            true            DELETE
           true            false           PUT
           false           true            NADA A FAZER
           false           false           POST
    */
    @Transient
    @Formula(select = "(select count(*) from prensamento p WHERE p.veiculo_id = veiculo_id AND p.data_hora BETWEEN inicio AND fim)")
    var pacotesReal: Long? = null

    @Transient
    @Formula(select = "(select count(distinct placa) from atividade_executada a, atividade_motorista m "
            + "where m.id = a.atividade_motorista_id AND a.jornada_id = \${ta}.id AND UPPER(m.nome) = 'CARREGAMENTO' )")
    var veiculosCarregados: Long? = null

    @Transient
    @Formula(select = "(select sum(qtde_pacotes) from atividade_executada a, atividade_motorista m "
            + "where m.id = a.atividade_motorista_id AND a.jornada_id = \${ta}.id AND UPPER(m.nome) = 'CARREGAMENTO' )")
    var pacotesCarregados: Long? = null
}