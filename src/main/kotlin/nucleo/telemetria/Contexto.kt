/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.prensa.Veiculo
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.Model
import java.io.Serializable
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class Contexto : Model, Serializable {
    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    var id: Int? = null

    @ManyToOne
    var veiculo: Veiculo? = null

    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    var ultimaAtualizacao: Date? = null
    var lat = 0.0
    var lon = 0.0
    var descricaoLocal: String? = null

    @JsonIgnore
    var ignicao = false
    var pacotes = 0

    @Temporal(TemporalType.TIMESTAMP)
    var ultimoPacote: Date? = null
    var atividade: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    var ultimaAtividade: Date? = null
    private val duration: Duration
        private get() {
            val now = Instant.now()
            val inst = ultimaAtualizacao!!.toInstant()
            return Duration.between(inst, now)
        }// Se mais de 6 horas

    // LIG,DES,OFF
    val status: String
        get() {
            // Se mais de 6 horas
            if (duration.toHours() > 6) {
                return "OFF"
            }
            return if (ignicao) "LIG" else "DES"
        }
    val hora: String
        get() {
            val data = DATA.format(ultimaAtualizacao)
            return if (data == DATA.format(Date())) {
                HORA_MM.format(ultimaAtualizacao)
            } else {
                DATA_HORA.format(ultimaAtualizacao)
            }
        }

    constructor(veiculo: Veiculo?) {
        this.veiculo = veiculo
    }

    fun setRegistro(reg: Registro) {
        veiculo = reg.veiculo
        ultimaAtualizacao = reg.dataHora
        lat = reg.latGraus
        lon = reg.lonGraus
        ignicao = reg.ignicao?:true
    }

    companion object {
        private val DATA = SimpleDateFormat("dd/MM")
        private val DATA_HORA = SimpleDateFormat("dd/MM HH:mm")
        private val HORA_MM = SimpleDateFormat("HH:mm")
    }
}