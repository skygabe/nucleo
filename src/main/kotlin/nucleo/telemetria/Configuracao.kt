/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.CustomModel
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Configuracao : CustomModel(), Serializable {
    @Id
    @GeneratedValue
    override //(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    var empregador: String? = null
    var cnpj: String? = null
    var endereco: String? = null
    var xmlTm30: String? = null
}