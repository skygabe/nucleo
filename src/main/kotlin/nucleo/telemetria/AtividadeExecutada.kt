/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.CustomModel
import br.com.hs1.morada.nucleo.prensa.*
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.NotNull
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
data class AtividadeExecutada(
        @Id
        @GeneratedValue
        override //(strategy = GenerationType.IDENTITY)
        var id: Int? = null,

        @ManyToOne
        @NotNull
        var jornada: Jornada? = null,

        @ManyToOne
        @NotNull
        var atividadeMotorista: AtividadeMotorista? = null,

        @Temporal(TemporalType.TIMESTAMP)
        @NotNull
        var inicio: Date? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var fim: Date? = null,

        @Column(length = 2000)
        var obs: String? = null,
        var qtdePacotes: Int? = null,

        @ManyToOne
        var material: Material? = null,
        var peso: Int? = null,
        var placa: String? = null,
        var kmIni: Int? = null,
        var kmFim: Int? = null,

        @ManyToOne
        var origem: Municipio? = null,

        @ManyToOne
        var destino: Municipio? = null,

        @ManyToOne
        var tipoManutencao: TipoManutencao? = null,

        @ManyToOne
        var componente: Componente? = null,
        var idExecucao: Long? = null,


        @Temporal(TemporalType.TIMESTAMP)
        @WhenModified
        @JsonIgnore
        var ultimaAtualizacao: Date? = null,

        @JsonIgnore
        override var sincronizado: Boolean? = null,

        @JsonIgnore
        override var enviado: Boolean? = null,

) : CustomModel(), Serializable, Sincronizavel {
    @SoftDelete
    @JsonIgnore
    override var excluido = false

}