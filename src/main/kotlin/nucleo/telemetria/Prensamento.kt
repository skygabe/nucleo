/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.CustomModel
import br.com.hs1.morada.nucleo.prensa.Veiculo
import br.com.hs1.morada.nucleo.seguranca.Pessoa
import io.ebean.annotation.History
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@History
data class Prensamento(
        @Id
        @GeneratedValue
        override var id: Int? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var dataHora: Date? = null,

        @ManyToOne
        var veiculo: Veiculo? = null,

        @ManyToOne
        var operador: Pessoa? = null,
        var sensor: Short = 0,
        override var sincronizado: Boolean? = null,
        override var enviado: Boolean? = null,

) : CustomModel(), Serializable, Sincronizavel {
    override val excluido: Boolean
    get() = false
}