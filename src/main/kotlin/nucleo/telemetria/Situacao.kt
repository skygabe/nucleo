/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import com.fasterxml.jackson.annotation.JsonFormat
import io.ebean.annotation.EnumValue

/**
 *
 * @author jcarlos
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class Situacao {
    @EnumValue("NM") NORMAL,
    @EnumValue("FR") FERIAS,
    @EnumValue("LM") LIC_MED,
    @EnumValue("FD") FERIADO,
    @EnumValue("AC") ABONADO_CHEFIA,
    @EnumValue("CP") COMPENSADO,
    @EnumValue("FT") FALTA,
    @EnumValue("DS") FOLGA,// em algum momento foi descanso semenal
    @EnumValue("FL") NAO_USAR;
}
