/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.prensa.Veiculo
import io.ebean.Model
import java.io.Serializable
import java.time.Instant
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

/**
 *
 * @author jcarlos
 */
@Entity
class Registro : Model, Serializable {
    @Id
    @GeneratedValue //(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @ManyToOne
    var veiculo: Veiculo? = null
    var timeStampLinux = 0
    var lat = 0
    var lon = 0
    var vel: Short? = null
    var sensores: Short? = null
    var rpm: Short? = null
    var ignicao: Boolean? = null
    var chaveMotorista: Int? = null
    var tipoRegistro: String? = null

    @ManyToOne
    var input: Input? = null

    constructor() {}

    override fun toString(): String {
        return "LAT:$lat LNG:$lon SENS:$sensores TR:$tipoRegistro"
    }

    constructor(veiculo: Veiculo?, input: Input?) {
        this.veiculo = veiculo
        this.input = input
    }

    val dataHora: Date
        get() = Date.from(Instant.ofEpochSecond(timeStampLinux.toLong()))

    fun setTimeStamp2004(timeStamp2004: Int) {
        timeStampLinux = 1072915200 + timeStamp2004
    }

    val latGraus: Double
        get() = lat / CONVERT_GRAU
    val lonGraus: Double
        get() = lon / CONVERT_GRAU
    val velKmHora: Short
        get() = Math.round(vel as Float / 10).toShort()

    companion object {
        private const val CONVERT_GRAU = 6000000.0
    }
}