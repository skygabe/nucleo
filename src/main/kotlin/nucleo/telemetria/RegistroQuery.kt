/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.prensa.Veiculo
import io.ebean.annotation.Sql
import java.io.Serializable
import java.util.*
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 *
 * @author jcarlos
 */
@Entity
@Sql
class RegistroQuery : Serializable {
    @ManyToOne
    val veiculo: Veiculo? = null

    @Temporal(TemporalType.TIMESTAMP)
    val dataHora: Date? = null
    val lat = 0.0
    val lon = 0.0
    val vel = 0f
    val ignicao: Boolean? = null
}