/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

/**
 *
 * @author jc
 */
interface Sincronizavel {
    var sincronizado: Boolean?
    val excluido: Boolean
    var enviado: Boolean?
    fun update()
    val id: Int?
}