/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class TipoHora(private val codigo: String) {
    HORA_EXTRA_100("0002"), HORA_EXTRA_50("0128"), COMPENSADO("0002"), FERIADO("0997"), DESCANSO("0003"), HORA_NORMAL("0128");

    fun getUrl(): String {
        return codigo
    }

    fun getDescricao(): String {
        return codigo
    }

    fun getCodigo(): String {
        return name
    }

    companion object {
        fun findByUrl(url: String): TipoHora? {
            for (t in values()) {
                if (t.codigo == url) {
                    return t
                }
            }
            return null
        }
    }
}