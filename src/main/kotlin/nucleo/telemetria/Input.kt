/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.telemetria

import br.com.hs1.morada.nucleo.prensa.Dispositivo
import br.com.hs1.morada.nucleo.prensa.TipoInput
import io.ebean.Model
import io.ebean.annotation.History
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
@History
class Input : Model(), Serializable {
    //    public static final QInput where = new QInput(db());
    @Id
    @GeneratedValue //(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    var dataHora: Date? = null
    var enderecoIP: String? = null

    @Column(columnDefinition = "TEXT")
    var dados: String? = null
    var tipo: TipoInput? = null
    var utilizado: Boolean? = null
    var versao: Int? = null

    @ManyToOne
    var dispositivo: Dispositivo? = null
}