/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.NotNull
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class MarcaModelo : CustomModel(), Serializable {
    @Id
    @GeneratedValue
    override var id: Int? = null

    @NotNull
    var nome: String? = null

    @SoftDelete
    var excluido = false

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    @Index
    @JsonIgnore
    var ultimaAtualizacao: Date? = null

}