/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 *
 * @author jcarlos
 */
@Entity
@Table(indexes = [Index(unique = true, columnList = "nome,excluido")])
class AtividadeMotorista : CustomModel(), Serializable {
    //    public static final QAtividadeMotorista where = new QAtividadeMotorista(db());
    @Id
    @GeneratedValue
    override var id: Int? = null
    var nome: @NotNull String? = null
    var nomeTablet: String? = null

    @Enumerated(EnumType.ORDINAL)
    var tipo: TipoAtividadeMotorista? = null

    @SoftDelete
    @JsonIgnore
    var excluido = false

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    @Index
    @JsonIgnore
    var ultimaAtualizacao: Date? = null
    var estatica = false
    var campos: String? = null

    override fun toString() = nome!!
}