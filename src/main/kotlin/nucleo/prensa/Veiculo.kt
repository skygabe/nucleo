/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 *
 * @author jcarlos
 */
@Entity
class Veiculo : CustomModel, Serializable {
    @Id
    @GeneratedValue
    override var id: Int? = null

    @Index(unique = true)
    var placa: @NotNull String? = null
    var descricao: String? = null
    var numChassi: String? = null

    @ManyToOne
    var marcaModelo: MarcaModelo? = null

    @Temporal(TemporalType.TIMESTAMP)
    var vencLicenciamento: Date? = null

    @SoftDelete
    @JsonIgnore
    var excluido = false

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    @Index
    @JsonIgnore
    var ultimaAtualizacao: Date? = null

    @OneToMany
    @JsonIgnore
    var dispositivos: List<Dispositivo>? = null

    constructor()

    override fun toString(): String {
        return descricao!!
    }

    constructor(descricao: String?) {
        this.descricao = descricao
        placa = descricao
    }

}