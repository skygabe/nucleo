/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import com.fasterxml.jackson.annotation.JsonFormat

/**
 *
 * @author jcarlos
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class TipoDispositivo(private val descricao: String) {
    TABLET("Tablet"), TM30("TM30");

    fun getDescricao(): String {
        return descricao
    }

    fun getCodigo(): String {
        return name
    }
}