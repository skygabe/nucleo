/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 *
 * @author jcarlos
 */
@Entity
data class TipoManutencao(
        @Id
        @GeneratedValue //(strategy = GenerationType.IDENTITY)
        override var id: Int? = null,
        var nome: @NotNull String? = null,

        @SoftDelete
        @JsonIgnore
        var excluido: Boolean = false,

        @Temporal(TemporalType.TIMESTAMP)
        @WhenModified
        @Index
        @JsonIgnore
        var ultimaAtualizacao: Date? = null,

        ) : CustomModel() {
    override fun toString() = nome?:""
}