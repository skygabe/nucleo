/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import java.text.Normalizer

/**
 *
 * @author jcarlos
 */
object StringUtils {
    fun retiraAcento(texto: String?): String {
        var str = Normalizer.normalize(texto, Normalizer.Form.NFD)
        str = str.replace("[^\\p{ASCII}]".toRegex(), "")
        str = str.replace("'".toRegex(), "")
        return str
    }
}