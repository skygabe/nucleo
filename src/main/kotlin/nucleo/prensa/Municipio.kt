/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import org.postgis.MultiPolygon
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class Municipio : CustomModel, Serializable{
    //    public static final QMunicipio where = new QMunicipio(db());
    @Id
    override var id: Int? = null
    var nome: String? = null
    var uf: String? = null

    @Transient
    @JsonIgnore
    var geom: MultiPolygon? = null

    @SoftDelete
    @JsonIgnore
    var excluido = false

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    @Index
    @JsonIgnore
    var ultimaAtualizacao: Date? = null
    override fun toString(): String {
        return nome!!
    }

    constructor(id: Int?, nome: String?, uf: String?) {
        this.id = id
        this.nome = nome
        this.uf = uf
    }

    val nomeCompleto: String
        get() = "$nome/$uf"
}