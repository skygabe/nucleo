/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import com.fasterxml.jackson.annotation.JsonFormat

/**
 *
 * @author jcarlos
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class TipoAtividadeMotorista(val descricao: String) {
    TRABALHO("Trabalho"),
    REFEICAO("Refeição"),
    DESCANSO_OBRIGATORIO("Descanso Obrigatório");

    override fun toString(): String {
        return descricao
    }

    fun getCodigo(): String {
        return name
    }
}