/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.NotNull
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 *
 * @author jcarlos
 */
@Entity
class Dispositivo : CustomModel, Serializable {
    @Id
    @GeneratedValue
    override var id: Int? = null

    @NotNull
    @Index(unique = true)
    var numSerie: String? = null
    var numLinhaTelefonica: String? = null

    @ManyToOne
    var veiculo: Veiculo? = null

    @Temporal(TemporalType.TIMESTAMP) //    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    var dataAquisicao: Date? = null

    @SoftDelete
    @JsonIgnore
    var excluido = false

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Index
    @WhenModified
    var ultimaAtualizacao: Date? = null
    var configuracao: String? = null
    var configAplicado = false
    var obs: String? = null
    var tipoDispositivo: TipoDispositivo? = null
    var versao: Int? = null

    @Temporal(TemporalType.TIMESTAMP)
    var ultimaConfiguracao: Date? = null

    @Temporal(TemporalType.TIMESTAMP)
    var ultimaConexao: Date? = null

    val isConfiguracaoVencida: Boolean
        get() {
            if (ultimaConfiguracao == null) {
                return true
            }
            val tempo = Date().time - ultimaConfiguracao!!.time
            return tempo > 24 * 60 * 60 * 1000
        }

    constructor(numSerie: String?, tipo: TipoDispositivo?) {
        this.numSerie = numSerie
        tipoDispositivo = tipo
    }
}