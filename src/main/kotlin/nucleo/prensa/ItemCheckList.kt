/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hs1.morada.nucleo.prensa

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.Index
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 *
 * @author jcarlos
 */
@Entity
class ItemCheckList : CustomModel() {
    //    public static final QItemCheckList where = new QItemCheckList(db());
    @Id
    @GeneratedValue
    override //(strategy = GenerationType.IDENTITY)
    var id: Int? = null
    var nome: @NotNull String? = null

    @SoftDelete
    @JsonIgnore
    var excluido = false

    @Temporal(TemporalType.TIMESTAMP)
    @WhenModified
    @Index
    @JsonIgnore
    var ultimaAtualizacao: Date? = null
}