package br.com.hs1.morada.nucleo.checklist

import br.com.hs1.morada.nucleo.CustomModel
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.time.Instant
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

enum class TipoPergunta {SIM_NAO,NUMERO,UNICA_ESCOLHA,MULTIPLA_ESCOLHA,DATA,SIM_NAO_MULTIPLA_ESCOLHA }
enum class CasoAlerta {SIM, NAO, NAO_HA, NEM_TODOS, FORA_FAIXA, CONFORME_ESCOLHA, VENCIDO }

@Entity
data class Pergunta(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id override var id: Int? = null,
        @field:NotNull
        var descricao: String? = null,
        @Enumerated(EnumType.STRING)
        @field:NotNull
        var tipoPergunta: TipoPergunta? = null,
        @field:NotNull
        @Enumerated(EnumType.STRING)
        var casoAlerta: CasoAlerta? = null,
        var solicitarJustificativa: Boolean? = null,
        var valorMinimoAceitavel: Float? = null,
        var valorMaximoAceitavel: Float? = null,
        var solicitarFoto: Boolean? = false,

        @OneToMany(mappedBy = "pergunta", orphanRemoval = true, cascade = [CascadeType.ALL])
        @JsonIgnoreProperties("pergunta")
        var escolhas: List<Escolha>? = null,

        @SoftDelete @JsonIgnore
        var excluido: Boolean = false,
        @Version
        var version: Long? = null,
        @WhenModified
        @Temporal(TemporalType.TIMESTAMP)
        var ultimaAtualizacao: Date? = null
): CustomModel() {
        fun addEscolhas(vararg lista: String) {
            escolhas =  lista.map { Escolha(texto = it, pergunta = this) }
        }

        val textoMinMax: String
                get() {
                        return when{
                                valorMaximoAceitavel == null && valorMaximoAceitavel  == null ->   "Não se aplica"
                                valorMinimoAceitavel != null && valorMaximoAceitavel != null -> "Entre $valorMinimoAceitavel e $valorMaximoAceitavel"
                                valorMinimoAceitavel != null ->  "Maior que $valorMinimoAceitavel"
                                valorMaximoAceitavel != null ->  "Menor que $valorMaximoAceitavel"
                                else -> ""
                        }
                }
}
