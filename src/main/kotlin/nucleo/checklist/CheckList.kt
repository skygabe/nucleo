package br.com.hs1.morada.nucleo.checklist

import br.com.hs1.morada.nucleo.CustomModel
import br.com.hs1.morada.nucleo.prensa.Municipio
import br.com.hs1.morada.nucleo.prensa.Veiculo
import br.com.hs1.morada.nucleo.seguranca.Pessoa
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRawValue
import com.fasterxml.jackson.annotation.JsonValue
import io.ebean.annotation.*
import org.postgis.Point
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
data class CheckList(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id override var id: Int?=null,
        @Temporal(TemporalType.TIMESTAMP) val dataHora: Date?=null,
        @Temporal(TemporalType.TIMESTAMP)
        @WhenCreated
        val dataHoraSincronizacao: Date? = null,
        @ManyToOne
       var operador: Pessoa?=null,
        @ManyToOne
       var veiculo: Veiculo?=null,

        @OrderBy("pergunta.id")
        @OneToMany(mappedBy = "checkList", cascade = [CascadeType.ALL], orphanRemoval = true)
       var respostas: MutableList<Resposta>? = null,
        var observacoes: String? = null,
        var lat: Double? = null,
        var lon: Double? = null,
        @ManyToOne
        var municipio: Municipio? = null,
        @SoftDelete
        @com.fasterxml.jackson.annotation.JsonIgnore
        var excluido: Boolean = false,
): CustomModel() {
    @Transient
    @Formula(select = "(select count(*) from resposta r WHERE r.check_list_id = \${ta}.id AND r.alerta is true)")
    var quantidadeAlertas: Long? = null
}
