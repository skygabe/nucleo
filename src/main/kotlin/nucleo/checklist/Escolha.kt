package br.com.hs1.morada.nucleo.checklist

import com.fasterxml.jackson.annotation.JsonIgnore
import io.ebean.annotation.NotNull
import io.ebean.annotation.SoftDelete
import io.ebean.annotation.WhenModified
import java.util.*
import javax.persistence.*

@Entity
data class Escolha(
        @GeneratedValue
        @Id var id: Int? = null,

        @field:NotNull
        var texto: String? = null,

        var alertar: Boolean? = null,

        @ManyToOne @field:NotNull
        var pergunta: Pergunta? = null,

        @SoftDelete @JsonIgnore
        var excluido: Boolean = false,

        @Version
        var version: Long? = null,

        @WhenModified
        @Temporal(TemporalType.TIMESTAMP)
        var ultimaAtualizacao: Date? = null
)