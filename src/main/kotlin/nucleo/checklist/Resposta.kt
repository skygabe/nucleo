package br.com.hs1.morada.nucleo.checklist

import br.com.hs1.morada.nucleo.CustomModel
import io.ebean.annotation.DbArray
import io.ebean.annotation.JsonIgnore
import io.ebean.annotation.SoftDelete
import java.io.Serializable
import java.text.SimpleDateFormat
import javax.persistence.*

val SDF = SimpleDateFormat("dd/MM/yyyy")

@Entity
data class Resposta(
    @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id override var id: Int?=null,
    @ManyToOne
    @JsonIgnore
        var checkList: CheckList?=null,
    @ManyToOne
        var pergunta: Pergunta?=null,
    @OneToMany(mappedBy = "resposta", orphanRemoval = true, cascade = [CascadeType.REMOVE])
        var fotos: MutableList<Foto>? = null,
    var valor: String?=null,
    var justificativa: String?=null,
    var alerta: Boolean = false,
    @DbArray
    var opcoes: Set<String>?=null,
    @SoftDelete
    @com.fasterxml.jackson.annotation.JsonIgnore
    var excluido: Boolean = false,

    ): CustomModel(), Serializable {

    @Transient
    var textoResposta: String?=null

    val respostaCalculada: String
        get()  {
            val resp = valor ?: ""
            val separador = if (resp.isNotEmpty() && opcoes?.isNotEmpty() == true) ": " else ""
            val lista = if (opcoes.isNullOrEmpty()) "" else opcoes?.joinToString(", ")
            return  "$resp$separador$lista"
        }

    val respostaEsperada: String?
        get() {
            return when(pergunta?.tipoPergunta){
                TipoPergunta.DATA -> "Até ${SDF.format(checkList?.dataHora)}"
                TipoPergunta.MULTIPLA_ESCOLHA ->
                    when(pergunta?.casoAlerta){
                        CasoAlerta.NEM_TODOS ->  pergunta?.escolhas?.joinToString { it.texto?:"" }
                        CasoAlerta.CONFORME_ESCOLHA -> pergunta?.escolhas?.filter { it.alertar != true }?.joinToString("|") { it.texto?:"" }
                        else -> ""
                    }
                TipoPergunta.NUMERO -> pergunta?.textoMinMax
                TipoPergunta.SIM_NAO, TipoPergunta.SIM_NAO_MULTIPLA_ESCOLHA ->
                    if(pergunta?.casoAlerta == CasoAlerta.SIM) "Não" else "Sim"
                TipoPergunta.UNICA_ESCOLHA -> pergunta?.escolhas?.filter { it.alertar != true }?.joinToString("|") { it.texto?:"" }
                else -> ""
            }
        }
    fun calcularResposta() {
        textoResposta = respostaCalculada
    }
}
