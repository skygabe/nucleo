package br.com.hs1.morada.nucleo.checklist

import br.com.hs1.morada.nucleo.CustomModel
import io.ebean.annotation.WhenModified
import java.util.*
import javax.persistence.*
import kotlin.jvm.Transient

@Entity
data class Foto(
    @GeneratedValue
    @Id override var id: Int?=null,

    @ManyToOne
    var resposta: Resposta?=null,

    @WhenModified
    @Temporal(TemporalType.TIMESTAMP)
    var dataHoraInclusao: Date?=null

): CustomModel() {
    @Lob
    @Basic(fetch=FetchType.LAZY)
    var imagem: ByteArray?=null

    @Transient
    var idServer: Int ?=null
}